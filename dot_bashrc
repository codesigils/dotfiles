#!/usr/bin/env bash

#
# ~/.bashrc
#

## ==============================
## = User Defined Vars
## ==============================
[ -x "$(command -v nvim)" ] && EDITOR=nvim
# EDITOR=/usr/bin/nvim
export EDITOR

export VISUAL=nvim
export BROWSER=librewolf
export VISUAL=vim
#export CC=/usr/bin/gcc
export CC=/usr/bin/gcc-11
export CXX=/usr/bin/gcc-11
export LANG="en_US.utf8"
export GIT_PS1_SHOWDIRTYSTATE=1

## ==============================
## = MANPAGER
## ==============================

## "bat" as a manpager
#export MANPAGER="sh -c 'col -bx | bat -l man -p'"

## "vim" as manpager
# export MANPAGER='/bin/bash -c "vim -MRn -c \"set buftype=nofile showtabline=0 ft=man ts=8 nomod nolist norelativenumber nonu noma\" -c \"normal L\" -c \"nmap q :qa<CR>\"</dev/tty <(col -b)"'

## "nvim" as manpager
#export MANPAGER="nvim -c 'set ft=man' -"

## SET VI MODE for cli
# Comment this line out to enable default emacs-like bindings
set -o vi
bind -m vi-command 'Control-l: clear-screen'
bind -m vi-insert 'Control-l: clear-screen'

## ==============================
## = Sell prompt & Colors
## ==============================

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h \W]\$ '

# For debianoids: set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
  debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
xterm-color | *-256color) color_prompt=yes ;;
esac

## For spaceship prompt
# [ -f "$HOME/.shell/.spaceshiprc" ] && . "$HOME/.shell/.spaceshiprc";

## Advanced command-not-found hook
# source /usr/share/doc/find-the-command/ftc.bash

## Colors
[ -f "$HOME/.shell/.colorsrc" ] && . "$HOME/.shell/.colorsrc"

## ==============================
## = WSL
## ==============================
#[ -f ~/.shell/.wslrc ] && . ~/.shell/.wslrc;

## ==============================
## = History - window - expantion
## ==============================

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth
HISTSIZE=1000
HISTFILESIZE=2000
export HISTTIMEFORMAT="%d/%m/%y %T "

# append to the history file, don't overwrite it
shopt -s histappend

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

## make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

## ==============================
## = SSH
## ==============================
## Use the following file to start an ssh session on every login.
## Otherwise store key in a pass manager like "seahorse".
# [[ -f ~/.shell/.sshrc ]] && . "$HOME/.shell/.sshrc";

## ==============================
## = Aliases
## ==============================
[ -f "$HOME/.shell/.aliasrc" ] && . "$HOME/.shell/.aliasrc"

## ==============================
## = Helpers - Completion - Fzf
## ==============================

# source /etc/profile.d/bash_completion.sh
[ -f "$HOME/.fzf.bash" ] && . "$HOME/.fzf.bash"

# Use bash-completion, if available
[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] &&
  . /usr/share/bash-completion/bash_completion

## Git bash completions
source /usr/share/bash-completion/completions/git

## ==============================
## = Paths
## ==============================
[ -f ~/.shell/.pathrc ] && . "$HOME/.shell/.pathrc"

## ==============================
## = opendoas completions
## ==============================
[ -x "$(command -v doas)" ] && complete -cf doas

## Torify your shell - Disable temporarily with "source torsocks off"
#. torsocks on

## Alacritty completions
[ -x "$(command -v alacritty)" ] && . "$HOME/.shell/alacritty"
